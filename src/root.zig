const builtin = @import("builtin");
const std = @import("std");

extern "c" fn pthread_attr_getstack(attr: *std.c.pthread_attr_t, base: *usize, size: *usize) std.c.E;
extern "c" fn pthread_get_stackaddr_np(thread: std.c.pthread_t) *anyopaque;
extern "c" fn pthread_get_stacksize_np(thread: std.c.pthread_t) usize;
extern "c" fn pthread_getattr_np(thread: std.c.pthread_t, attr: *std.c.pthread_attr_t) std.c.E;
extern "c" fn pthread_stackseg_np(thread: std.c.pthread_t, stack: *std.c.stack_t) std.c.E;
// GetCurrentThreadStackLimits is only supported on Windows 8 and above so we look it up at runtime
// instead of declaring it as `extern` here which allows us to return error.NotImplemented in case
// it's not available.
const GetCurrentThreadStackLimitsFn = fn (
    low_limit: *std.os.windows.ULONG_PTR,
    high_limit: *std.os.windows.ULONG_PTR,
) callconv(std.os.windows.WINAPI) void;

pub const StackInfo = struct {
    base: usize,
    size: usize,

    pub const Error = error{ NotImplemented, PthreadError } || std.posix.UnexpectedError || std.DynLib.Error;

    pub fn init() Error!StackInfo {
        var base: usize = undefined;
        var size: usize = undefined;
        switch (builtin.os.tag) {
            .linux, .haiku, .freebsd, .netbsd, .solaris => {
                var attr: std.c.pthread_attr_t = undefined;
                if (pthread_getattr_np(std.c.pthread_self(), &attr) != .SUCCESS) {
                    return error.PthreadError;
                }
                if (pthread_attr_getstack(&attr, &base, &size) != .SUCCESS) {
                    return error.PthreadError;
                }
                if (std.c.pthread_attr_destroy(&attr) != .SUCCESS) {
                    return error.PthreadError;
                }
            },
            .openbsd => {
                var stack: std.c.stack_t = undefined;
                if (pthread_stackseg_np(std.c.pthread_self(), &stack) != .SUCCESS) {
                    return error.PthreadError;
                }
                base = @intFromPtr(stack.sp) - stack.size;
                size = stack.size;
            },
            .macos, .ios, .watchos, .tvos => {
                const limit = pthread_get_stackaddr_np(std.c.pthread_self());
                size = pthread_get_stacksize_np(std.c.pthread_self());
                base = @intFromPtr(limit) - size;
            },
            .windows => {
                var kernel32 = try std.DynLib.open("kernel32.dll");
                defer kernel32.close();
                if (kernel32.lookup(
                    *const GetCurrentThreadStackLimitsFn,
                    "GetCurrentThreadStackLimits",
                )) |GetCurrentThreadStackLimits| {
                    var low_limit: std.os.windows.ULONG_PTR = undefined;
                    var high_limit: std.os.windows.ULONG_PTR = undefined;
                    GetCurrentThreadStackLimits(&low_limit, &high_limit);
                    base = low_limit;
                    size = high_limit - low_limit;
                } else {
                    return error.NotImplemented;
                }
            },
            else => return error.NotImplemented,
        }
        if (builtin.os.tag == .linux and builtin.abi.isMusl()) {
            // Musl reports the current stack size for the main thread, not the total. See:
            // - https://bugs.webkit.org/show_bug.cgi?id=225099
            // - https://github.com/SerenityOS/serenity/issues/16681
            if (std.os.linux.getpid() == std.os.linux.gettid()) {
                const current_limit = base + size;
                const rlimit = try std.posix.getrlimit(.STACK);
                size = @truncate(rlimit.cur);
                size -= std.heap.pageSize(); // Account for a guard page
                base = current_limit - size;
            }
        }
        return .{ .base = base, .size = size };
    }
};

test StackInfo {
    if (StackInfo.init()) |stack_info| {
        try std.testing.expect(stack_info.size > 0);
        try std.testing.expect(stack_info.base > 0);
        try std.testing.expect(stack_info.base < @frameAddress());
    } else |err| {
        try std.testing.expectError(error.NotImplemented, @as(StackInfo.Error!StackInfo, err));
    }
}
