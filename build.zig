const std = @import("std");

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const link_libc = switch (target.result.os.tag) {
        // pthread_attr_getstack
        .linux, .haiku, .freebsd, .netbsd, .solaris => true,
        // pthread_stackseg_np
        .openbsd => true,
        // pthread_get_stackaddr_np, pthread_get_stacksize_np
        .macos, .ios, .watchos, .tvos => true,
        // GetCurrentThreadStackLimits is from kernel32, not libc
        .windows => null,
        else => null,
    };

    _ = b.addModule("stackinfo", .{
        .root_source_file = b.path("src/root.zig"),
        .link_libc = link_libc,
    });

    const unit_tests = b.addTest(.{
        .root_source_file = b.path("src/root.zig"),
        .target = target,
        .optimize = optimize,
        .link_libc = link_libc,
    });
    const run_unit_tests = b.addRunArtifact(unit_tests);

    const test_step = b.step("test", "Run unit tests");
    test_step.dependOn(&run_unit_tests.step);
}
