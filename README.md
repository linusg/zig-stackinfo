# zig-stackinfo

A small cross-platform Zig library to get the native stack base and size.

## API

```zig
pub const StackInfo = struct {
    base: usize,
    size: usize,

    pub const Error = error{ NotImplemented, PthreadError } || std.posix.UnexpectedError;

    pub fn init() Error!StackInfo {
        // ...
    }
};
```

## Implemented platforms

- FreeBSD
- Haiku
- Linux
- macOS/iOS/watchOS/tvOS
- NetBSD
- OpenBSD
- Solaris
- Windows

Note that most of these have not been manually tested.
